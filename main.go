package main

import (
  "net/http"
  "fmt"
  "github.com/prometheus/client_golang/prometheus"
  "github.com/prometheus/client_golang/prometheus/promhttp"
  "log"
  "time"
  "math/rand"
)

var (
  counter = prometheus.NewCounter(
     prometheus.CounterOpts{
        Namespace: "hb",
        Name:      "request_total",
        Help:      "Http requests total counter.",
     })
)

func handler(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Hello from @dyrnade")
    counter.Inc()
}

func main() {
  rand.Seed(time.Now().Unix())

  http.Handle("/metrics", promhttp.Handler())
  http.HandleFunc("/", handler)

  prometheus.MustRegister(counter)

  log.Println("Server is starting...")
  log.Fatal(http.ListenAndServe(":11130", nil))
}
