Project link: https://gitlab.com/dyrnade/hb-server

Prerequesites:
  - gitlab account
  - go version go1.13.5
  - docker version 19.03.12

The project is a simple http application which has two endpoints. The application is served at port 11130.
  - / returns "@dyrnade"
  - /metrics returns default prometheus metrics and additionally a custom metrics named "hb_requests_total". This metric's value is increased by viewing / endpoint.


The project has three parts in terms of code.
  - main.go: The application's code.
  - Dockerfile: Image definition to build project. An intermediate image builds project first and then a new image created with that artifact. 
    Image is named with short commit sha($IMAGE_NAME:$CI_COMMIT_SHORT_SHA). 
  - .gitlab-ci.yml: CI/CD definition of the project. It has two stages;
     1. upload-image: builds image and pushes it to $DOCKERHUB_USER/$CI_PROJECT_NAME repository on Dockerhub.
     2. update-image-tag: triggers the repository which contains Kubernetes cluster code. 
        The short commit sha is sent as an argument(IMAGE_TAG=$CI_COMMIT_SHORT_SHA) with trigger and other pipeline will deploy image with this tag to the Kubernetes cluster.

IMPORTANT: You should set DOCKERHUB_USER and DOCKERHUB_TOKEN variables in https://gitlab.com/USERNAME/hb-server/-/settings/ci_cd that are taken from Dockerhub.
           You should also set TRIGGERED_PROJECT_ID to k8s-hb-server project's id on your account.
