FROM golang:1.14-alpine3.11 AS builder
ARG REPO_URL
COPY . /go/src/$REPO_URL
WORKDIR /go/src/$REPO_URL

RUN apk add --no-cache git && go get && go build -o ./server

### app image
FROM alpine:3.9.2
ARG REPO_URL
COPY --from=builder /go/src/$REPO_URL/server /server
RUN adduser -H -D builder
USER builder

CMD [ "/server" ]
